# app.zsh

# app function
app() {
    local cmd=$1
    shift
    local args="$@"
    if [ -d "$PWD/.scripts" ]; then
        local script_path="$PWD/.scripts/$cmd.sh"
        if [ -f "$script_path" ]; then
            sh "$script_path" $args
        else
            echo "No script named $cmd.sh found in the .scripts folder."
        fi
    else
        echo "No .scripts folder found in the current directory."
    fi
}

# Generic completion function
_generic_complete() {
    local -a script_commands

    # Check if .scripts folder exists in the current directory
    if [ -d "$PWD/.scripts" ]; then
        for script in $PWD/.scripts/*.sh; do
            script_name=$(basename "$script" .sh)
            description=$(sed -n '2p' "$script")
            if [[ -z "$description" ]]; then
                description="Run the $script_name script"
            fi
            script_commands+=("$script_name:$description")
        done
    fi

    if [[ $CURRENT -eq 2 ]]; then
        _describe 'command' script_commands
    fi
}

# Associate the completion function with the app command
compdef _generic_complete app
